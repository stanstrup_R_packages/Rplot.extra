# Rplot.extra
R functions for various plots

```r
install.packages("devtools")
devtools::install_git("https://gitlab.com/R_packages/Rplot.extra.git")
```